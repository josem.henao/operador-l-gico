class Expression:
    def __init__(self, expression: str):
        self.expression = expression
        self.n = 0
        self.max = len(self.expression)

    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n < self.max:
            self.n += 1
            return self.expression[self.n - 1]
        else:
            return None

    def __add__(self, other: str):
        return Expression(self.expression + other)

    def __getitem__(self, item):
        return Expression(self.expression[item])

    def __hasnext__(self):
        return self.n < self.max

    def __repr__(self):
        return self.expression

    def __len__(self):
        return len(self.expression)
