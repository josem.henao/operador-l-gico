general_operators = ['>', '=', '|', '&']
general_propositions = ['p', 'q', 'r']
valid_grouping = {'open': '(', 'close': ')'}

equivalent_booleans = {
    True: "v",
    False: "f"
}


samples = [
    "(p) & (p)",
    "(q)",
    "p | (q)",
    "(p | p) & p",
    "(p>q)|p",
    "((p = q) | r) = (r & (q & p)) "
]
