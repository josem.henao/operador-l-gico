import re

from expression import Expression
from variables import valid_grouping, general_operators, general_propositions


class Util:

    @classmethod
    def generate_combinations(cls, propositions: list) -> dict:
        """
        Generates combinations of 1 y 0 depending on the number of propositions provided
        :param: list
            prepositions = ['a', 'b', 'c']
        :return: dict
            {
                'a': [1,1,1,1,0,0,0,0],
                'b': [1,1,0,0,1,1,0,0],
                'c': [1,0,1,0,1,0,1,0]
            }
        """
        pass

    @classmethod
    def build_operation_tree(cls, expression: Expression) -> dict:
        """
        Split an expression into groups of expressions
        :param: Expression
            'p>((q|r)=s)'
        :return: dict
            {
                'prop_a': 'p',
                'op': '>',
                'prop_b': {
                    'prop_a': {
                        'prop_a': 'q',
                        'op': '|',
                        'prop_b': 'r'
                    }
                    'op': '=',
                    'prob_b': 's'
                }
            }
        """
        operation = {'prop_a': None, 'op': None, 'prop_b': None}
        while c := expression.__next__():
            if c in general_propositions and not operation['prop_a']:
                operation['prop_a'] = c
            elif c in general_operators:
                operation['op'] = c
            elif c in general_propositions and not operation['prop_b']:
                operation['prop_b'] = c
                if expression.__next__():
                    raise Exception(f"Unable to build a logic operation from the expresion '{expression}'")
                if operation['prop_a'] and operation['op'] and operation['prop_b']:
                    return operation
                else:
                    raise Exception(f"Unable to build a B operand with {expression}")
            elif c == valid_grouping['open']:
                g_open = 1
                g_close = 0
                sub_exp = Expression(valid_grouping['open'])
                while ch := expression.__next__():
                    if ch != valid_grouping['close']:
                        if ch == valid_grouping['open']:
                            g_open += 1
                        sub_exp += ch
                    else:
                        g_close += 1
                        sub_exp += ch
                        if g_open == g_close:
                            if not operation['prop_a']:
                                operation['prop_a'] = Util.build_operation_tree(sub_exp[1:-1])
                                break
                            elif not operation['prop_b']:
                                operation['prop_b'] = Util.build_operation_tree(sub_exp[1:-1])
                                if expression.__next__():
                                    raise Exception(f'Unable to determinate the priority in the expresion {expression}')
                                if operation['prop_a'] and operation['op'] and operation['prop_b']:
                                    return operation
                                else:
                                    raise Exception(f"Unable to build a B operand with {expression}")

            else:
                raise Exception(f"Unable to evaluate {c}")
        return operation

    @classmethod
    def replace_extra_grouping_single_prop(cls, expression: str):
        exp_validator = f"\\{valid_grouping['open']}[{''.join(general_propositions)}]\\{valid_grouping['close']}"
        while to_replace := re.findall(exp_validator, expression):
            for exp in to_replace:
                expression = expression.replace(exp, exp[1:-1])
        return expression

    @classmethod
    def replace_extra_grouping_in_operation(cls, expression: str):
        exp_validator = f"\\{valid_grouping['open']}\\{valid_grouping['open']}[{''.join(general_propositions)}][{''.join(general_operators)}]" \
                        f"[{''.join(general_propositions)}]\\{valid_grouping['close']}\\{valid_grouping['close']}"
        while to_replace := re.findall(exp_validator, expression):
            for exp in to_replace:
                expression = expression.replace(exp, exp[1:-1])
        return expression
