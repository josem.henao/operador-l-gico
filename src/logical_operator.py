from variables import general_propositions, general_operators, equivalent_booleans


class GenericFunctions:

    @classmethod
    def operate(cls, p: bool, operator: str, q: bool) -> bool:

        if operator == "&":
            return p and q
        elif operator == "|":
            return p or q
        elif operator == "=":
            return p == q
        elif operator == ">":
            "The same than -> "
            return not p or q
        else:
            raise Exception(f"Operator '{operator}' unknown.")

    @classmethod
    def remove_one_useless_parenthesis(cls, _expression: dict) -> dict:
        """
        Objective: Delete just one useless parentheses

        Example: Based on _expression.values()

            Input : ( ( ( ( p ) ) ) ) | ( q )
            Output:   ( ( ( p ) ) )   | ( q )

        :param _expression: Is a dictionary, where the keys are the positions (these positions are necessary to print
                            the truth table)
        :return: the same expression, but with one useless parentheses less
        """

        _keys = list(_expression.keys())
        _values = list(_expression.values())
        _in = 0

        while _in < len(_keys)-2:
            if _values[_in] == "(" and isinstance(_values[_in+1], bool) and _values[_in+2] == ")":
                del _expression[_keys[_in]]
                del _expression[_keys[_in + 2]]
                return _expression
            _in += 1

        return _expression

    @classmethod
    def remove_all_useless_parentheses(cls, _expression: dict) -> dict:
        """
        Use the function GenericFunctions.remove_one_useless_parenthesis() until there are no useless parentheses.

        Example: Based on _expression.values()

                Input : ( ( ( ( p ) ) ) ) | ( q )
                Output:   p | q

        :param _expression:
        :return:
        """

        length = len(_expression.keys())
        flag = True
        while flag:
            _expression = cls.remove_one_useless_parenthesis(_expression)
            if length != len(_expression.keys()):
                length = len(_expression.keys())
            else:
                flag = False

        return _expression


class Operation:

    def __init__(self, expression: dict):
        self.expression = expression
        self.str_chain = [equivalent_booleans[i] if isinstance(i, bool) else i for i in self.expression.values()]

    def full_operation(self):
        """
        The pattern is simple:
            1. Remove all useless parentheses
            2. And do operation, [boolean, operator, boolean]
            3. Run again step 1 and 2 until get expression with dimension one.
            (((p>q)|(q>r))|(p&p))
        """

        while len(self.expression.keys()) != 1:
            _expression = GenericFunctions.remove_all_useless_parentheses(self.expression)
            _expression = self._do_operation()

        return " ".join(self.str_chain).replace("(", ".").replace(")", ".")

    def _do_operation(self) -> dict:
        """
        loops through the structure 3 by 3, moving position by position until get [boolean, operator, boolean]
            and finally make the operation.
        Example:

        :return:
        """

        _keys = list(self.expression.keys())
        _values = list(self.expression.values())
        _in = 0

        while _in < len(_keys)-2:
            if isinstance(_values[_in], bool) and _values[_in+1] in general_operators and isinstance(_values[_in+2],
                                                                                                     bool):
                self.expression[_keys[_in + 1]] = GenericFunctions.operate(
                    _values[_in], _values[_in + 1], _values[_in + 2]
                )
                self.str_chain[_keys[_in + 1]] = equivalent_booleans[self.expression[_keys[_in + 1]]]
                del self.expression[_keys[_in]]
                del self.expression[_keys[_in + 2]]

                return self.expression

            _in += 1

        return self.expression


class LogicalOperator:

    expression = None
    input_propositions = {}
    propositions = []
    all_operations = []

    @classmethod
    def reformat_expression(cls, _expression: str):

        cls.expression = list(_expression.replace(" ", ""))

        # Select just the preposition which are in the expression.
        cls.propositions = [prep for prep in general_propositions if prep in _expression]
        cls.propositions.reverse()

    @classmethod
    def generate_table_inputs(cls):
        """
            Example:
                cls.propositions -> ['q', 'p']
            Result:
                cls.input_propositions -> {
                                        'q': [True, False, True, False],
                                        'p': [True, True, False, False]
                                    }
        """

        for i in range(0, len(cls.propositions)):
            cls.input_propositions[cls.propositions[i]] = ([True] * (2 ** i) + [False] * (2 ** i)) * \
                                                          (2 ** (len(cls.propositions) - i - 1))

    @classmethod
    def create_all_possibilities(cls) -> None:
        """
        Create all_operations list based on expressions variable and input_propositions dictionary.

        example:

        cls.expression:
            ['(', '(', 'p', ')', ')', '|', '(', 'q', ')']

        structured generated:
            cls.all_operations = [
                {0: '(', 1: '(', 2: True, 3: ')', 4: ')', 5: '|', 6: '(', 7: True, 8: ')'}
                {0: '(', 1: '(', 2: True, 3: ')', 4: ')', 5: '|', 6: '(', 7: False, 8: ')'}
                {0: '(', 1: '(', 2: False, 3: ')', 4: ')', 5: '|', 6: '(', 7: True, 8: ')'}
                {0: '(', 1: '(', 2: False, 3: ')', 4: ')', 5: '|', 6: '(', 7: False, 8: ')'}
            ]
        """

        for i in range(2 ** len(cls.propositions)):
            dict_op = {}
            list_op = [cls.input_propositions[m][i] if m in general_propositions else m for m in cls.expression]
            for ind, exp in enumerate(list_op):
                dict_op[ind] = exp

            cls.all_operations.append(dict_op)

    @classmethod
    def execute(cls, logic_operation: str):

        LogicalOperator.reformat_expression(logic_operation)
        LogicalOperator.generate_table_inputs()
        LogicalOperator.create_all_possibilities()

        # Finally, Display results
        print("----------------------------------------------")
        print("----------------- TRUTH TABLE ----------------")
        print("----------------------------------------------")
        print(" ".join(list(logic_operation.replace(" ", ""))))

        for op in LogicalOperator.all_operations:
            print(Operation(op).full_operation())
