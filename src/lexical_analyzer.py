import re

from expression import Expression
from util import Util
from variables import general_propositions, general_operators, valid_grouping


class LexicalAnalysis:
    """
    Ensure regular expression is written correctly.
    """

    _propositions_count_allowed = 6
    _exp: Expression = None
    _valid_chars = general_propositions + [valid_grouping['open']] + [valid_grouping['close']] + general_operators
    print(f"_valid_chars -> {_valid_chars}")

    @classmethod
    def expression_is_correct(cls, expression: Expression) -> bool:
        cls._exp = expression

        assert cls._contains_correct_chars() is True, Exception(
            f"The expression doesnt contains correct chars use {cls._valid_chars} instead")

        assert cls._same_grouping_count() is True, Exception(
            f"The count of '{valid_grouping['open']}' isn't the same of '{valid_grouping['close']}'")

        assert cls._has_adjacent_operators() is False, Exception("Adjacent operators found")

        assert cls._has_adjacent_propositions() is False, Exception("Adjacent propositions found")

        assert cls._has_adjacent_grouping_symbols() is False, Exception("Adjacent grouping symbols found")

        assert cls._correct_propositions_usage_count() is True, Exception(
            f"Use at most {cls._propositions_count_allowed} propositions")

        assert cls._generate_valid_tree() is True, Exception(
            "Unable to build a valid graph of operations")

        return True

    @classmethod
    def _contains_correct_chars(cls) -> bool:
        found_invalid = list(filter(lambda x: x not in cls._valid_chars, list(cls._exp.expression)))
        list(map(lambda x: print(f"-- Invalid char: {x}"), found_invalid))
        return found_invalid == []

    @classmethod
    def _same_grouping_count(cls) -> bool:
        grouping_open = list(filter(lambda x: x == valid_grouping['open'], cls._exp.expression))
        grouping_close = list(filter(lambda x: x == valid_grouping['close'], cls._exp.expression))
        print(f"-- grouping_open - {grouping_open}\n-- grouping_close - {grouping_close}")
        return len(grouping_open) == len(grouping_close)

    @classmethod
    def _generate_valid_tree(cls) -> bool:
        return True if Util.build_operation_tree(cls._exp) else False

    @classmethod
    def _correct_propositions_usage_count(cls):
        props = list(filter(lambda x: x in general_propositions, cls._exp.expression))
        return len(props) <= cls._propositions_count_allowed

    @classmethod
    def _has_adjacent_grouping_symbols(cls) -> bool:
        return True if re.findall(r"\)\(", cls._exp.expression) else False

    @classmethod
    def _has_adjacent_grouping_symbols(cls) -> bool:
        return True if re.findall(r"\(\)", cls._exp.expression) else False

    @classmethod
    def _has_adjacent_propositions(cls) -> bool:
        props = "".join(general_propositions)
        return True if re.findall(r"[" + props + "]{2}", cls._exp.expression) else False

    @classmethod
    def _has_adjacent_operators(cls):
        ops = "".join(general_operators)
        return True if re.findall(r"[" + ops + "]{2}", cls._exp.expression) else False
