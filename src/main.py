from util import Util


def main(expression: str):
    from lexical_analyzer import LexicalAnalysis
    from expression import Expression
    from logical_operator import LogicalOperator
    expression_cleaned = Util.replace_extra_grouping_single_prop(expression)
    expression_cleaned = Util.replace_extra_grouping_in_operation(expression_cleaned)

    print(f"Evaluating -> {expression}")
    assert LexicalAnalysis.expression_is_correct(Expression(expression_cleaned)) is True

    print(f"-- {expression} lexically correct")
    LogicalOperator.execute(expression)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Runs a test over logical expression.", )
    parser.add_argument("-e", "--expression",
                        help="The expresion to be evaluated",
                        required=True
                        )

    args = parser.parse_args()
    main(args.expression)
